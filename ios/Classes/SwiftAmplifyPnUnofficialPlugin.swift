import Flutter
import UIKit
import AWSPinpoint
import Amplify
import AmplifyPlugins

public class SwiftAmplifyPnUnofficialPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "amplify_pn_unofficial", binaryMessenger: registrar.messenger())
    let instance = SwiftAmplifyPnUnofficialPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)

    registrar.addApplicationDelegate(instance)
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch (call.method) {
    case "initialize":
      do {
        let plugin = try Amplify.Analytics.getPlugin(for: PluginKey("awsPinpointAnalyticsPlugin")) as! AWSPinpointAnalyticsPlugin
        let pinpoint = plugin.getEscapeHatch()
        notificationManager = pinpoint.notificationManager
        result(true)
      } catch {
        result(false)
      }
      break
    case "registerDeviceToken":
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        if (settings.authorizationStatus == .authorized) {
          DispatchQueue.main.async {
            UIApplication.shared.registerForRemoteNotifications()
          }
          result(true)
        } else {
          result(false)
        }
      }
      break
    default:
      result(FlutterMethodNotImplemented)
    }
  }

  private var notificationManager: AWSPinpointNotificationManager?

  public func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
    let token = tokenParts.joined()
    print("Device Token: \(token)")
    
    notificationManager?.interceptDidRegisterForRemoteNotifications(withDeviceToken: deviceToken)
  }

  public func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    print("Failed to register: \(error)")
  }
  
  public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    // Handle foreground push notifications
    notificationManager?.interceptDidReceiveRemoteNotification(notification.request.content.userInfo)
    completionHandler(.badge)
  }
  
  public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
    // Handle background and closed push notifications
    notificationManager?.interceptDidReceiveRemoteNotification(response.notification.request.content.userInfo)
    completionHandler()
  }
}
