#import "AmplifyPnUnofficialPlugin.h"
#if __has_include(<amplify_pn_unofficial/amplify_pn_unofficial-Swift.h>)
#import <amplify_pn_unofficial/amplify_pn_unofficial-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "amplify_pn_unofficial-Swift.h"
#endif

@implementation AmplifyPnUnofficialPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftAmplifyPnUnofficialPlugin registerWithRegistrar:registrar];
}
@end
