package vn.kaypay.amplify_pn_unofficial

import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import com.google.firebase.messaging.FirebaseMessaging

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** AmplifyPnUnofficialPlugin */
class AmplifyPnUnofficialPlugin: FlutterPlugin, MethodCallHandler {
  companion object {
    const val TAG = "amplify_pn_unofficial"
  }

  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel

  private lateinit var context: Context

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "amplify_pn_unofficial")
    channel.setMethodCallHandler(this)

    context = flutterPluginBinding.applicationContext
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (call.method) {
        "initialize" -> {
          result.success(true)
        }
        "registerDeviceToken" -> {
          val pinpointManager = PinpointManagerHolder.getInstance(context)
          FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
              task.exception?.let { Log.w(TAG, "FirebaseMessaging.token failed.", it) }
              result.success(false)
            } else {
              val token = task.result
              Log.d(TAG, "FirebaseMessaging.token -> $token")
              pinpointManager.notificationClient.registerDeviceToken(token)
              result.success(true)
            }
          }
        }
        else -> {
          result.notImplemented()
        }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}
