package vn.kaypay.amplify_pn_unofficial

import android.content.Context
import android.util.Log
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobile.client.Callback
import com.amazonaws.mobile.client.UserStateDetails
import com.amazonaws.mobile.config.AWSConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointConfiguration
import com.amazonaws.mobileconnectors.pinpoint.PinpointManager

class PinpointManagerHolder {
    companion object {
        const val TAG = "amplify_pn_unofficial"

        private var instance: PinpointManager? = null

        fun getInstance(context: Context): PinpointManager {
            val existingInstance = instance
            if (existingInstance != null) {
                return existingInstance
            }

            val awsClient = AWSMobileClient.getInstance()

            var awsConfig = awsClient.configuration
            if (awsConfig == null) {
                awsConfig = AWSConfiguration(context)
                awsClient.initialize(context, awsConfig, object : Callback<UserStateDetails> {
                    override fun onResult(result: UserStateDetails?) {
                        Log.i(TAG, "initialize -> $result")
                    }

                    override fun onError(e: Exception?) {
                        Log.e(TAG, "initialize -> $e")
                    }
                })
            }

            val pinpointConfig = PinpointConfiguration(context, awsClient, awsConfig)
            val newInstance = PinpointManager(pinpointConfig)
            instance = newInstance

            return newInstance
        }
    }
}