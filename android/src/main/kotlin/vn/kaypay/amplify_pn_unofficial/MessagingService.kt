package vn.kaypay.amplify_pn_unofficial

import android.util.Log
import com.amazonaws.mobileconnectors.pinpoint.targeting.notification.NotificationClient
import com.amazonaws.mobileconnectors.pinpoint.targeting.notification.NotificationDetails
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MessagingService : FirebaseMessagingService() {
    companion object {
        const val TAG = "amplify_pn_unofficial"
    }

    private val notificationClient: NotificationClient by lazy {
        PinpointManagerHolder.getInstance(applicationContext).notificationClient
    }

    override fun onNewToken(token: String) {
        Log.i(TAG, "onNewToken $token")

        super.onNewToken(token)
        notificationClient.registerDeviceToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val notificationDetails = NotificationDetails.builder()
                .from(remoteMessage.from)
                .mapData(remoteMessage.data)
                .intentAction(NotificationClient.FCM_INTENT_ACTION)
                .build()
        val pushResult = notificationClient.handleNotificationReceived(notificationDetails)
        Log.i(TAG, "handleNotificationReceived -> $pushResult")
    }
}