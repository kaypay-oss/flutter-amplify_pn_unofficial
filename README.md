# amplify_pn_unofficial

Unofficial package to receive push notification with Amplify & AWS PinPoint.

## Getting Started

1. Setup FCM (`amplify add notifications`)
    - Follow https://docs.amplify.aws/sdk/push-notifications/getting-started/q/platform/android/
    - Make sure to copy the `google-services.json` file into `./android/app` directory
    - And update the `build.gradle`s
2. Setup APNS
    - Follow https://docs.amplify.aws/sdk/push-notifications/getting-started/q/platform/ios/
    - Make sure to update the plist and entitlement files
3. Configure Amplify

```dart
await Amplify.addPlugins([
  AmplifyAnalyticsPinpoint(),
  AmplifyAuthCognito(),
]);
await Amplify.configure(amplifyconfig);
await AmplifyPnUnofficial.initialize();
```

4. Call `AmplifyPnUnofficial.registerDeviceToken()` when user is logged in
  - If needed, set a Analytics property to trigger endpoint address sync immediately

### Android

There is a issue that crashes Android app if it receives a background notification (app is terminated). Use a script to populate the `./android/app/src/main/res/raw/awsconfiguration.json` file from `lib/amplifyconfiguration.dart`, something like this:

```dart
import 'dart:convert';

import '../lib/amplifyconfiguration.dart';

void main() {
  final map = jsonDecode(amplifyconfig);
  final awsCognitoAuthPlugin = map['auth']['plugins']['awsCognitoAuthPlugin'];
  print(jsonEncode(awsCognitoAuthPlugin));
}
```

```bash
dart ../scripts/print-aws-configuration.dart > ./android/app/src/main/res/raw/awsconfiguration.json
```
