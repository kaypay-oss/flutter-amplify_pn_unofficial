import 'dart:async';

import 'package:amplify_analytics_pinpoint/amplify_analytics_pinpoint.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_authenticator/amplify_authenticator.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:amplify_pn_unofficial/amplify_pn_unofficial.dart';
import 'package:amplify_pn_unofficial_example/amplifyconfiguration.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  PermissionStatus? _notificationStatus;

  @override
  void initState() {
    super.initState();
    _configureAmplify();
    _initNotificationStatus();
  }

  Future<void> _configureAmplify() async {
    await Amplify.addPlugins([
      AmplifyAnalyticsPinpoint(),
      AmplifyAuthCognito(),
    ]);
    await Amplify.configure(amplifyconfig);
    await AmplifyPnUnofficial.initialize();
  }

  Future<void> _initNotificationStatus() async {
    final status = await Permission.notification.status;
    if (!mounted) return;
    setState(() {
      _notificationStatus = status;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget built = MaterialApp(
      builder: Authenticator.builder(),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: const [
                  null,
                  PermissionStatus.permanentlyDenied,
                  PermissionStatus.granted,
                ].contains(_notificationStatus)
                    ? null
                    : _requestNotificationPermission,
                child: const Text('Request notification permission'),
              ),
              Text(
                'Current status: ${_notificationStatus?.name}\n',
                style: Theme.of(context).textTheme.caption,
              ),
              ElevatedButton(
                onPressed: () => _setCustom1(_custom1Value1),
                child: const Text('Set $_custom1=$_custom1Value1'),
              ),
              ElevatedButton(
                onPressed: () => _setCustom1(_custom1Value2),
                child: const Text('Set $_custom1=$_custom1Value2'),
              ),
              ElevatedButton(
                onPressed: _notificationStatus == PermissionStatus.granted
                    ? AmplifyPnUnofficial.registerDeviceToken
                    : null,
                child: const Text('Register device token'),
              )
            ],
          ),
        ),
      ),
    );

    built = Authenticator(child: built);

    return built;
  }

  static const _custom1 = 'custom1';
  static const _custom1Value1 = 'value1';
  static const _custom1Value2 = 'value2';

  Future<void> _setCustom1(String value) async {
    final user = await Amplify.Auth.getCurrentUser();
    final userProfile = AnalyticsUserProfile();

    final p = userProfile.properties = AnalyticsProperties();
    p.addStringProperty(_custom1, value);

    await Amplify.Analytics.identifyUser(
      userId: user.userId,
      userProfile: userProfile,
    );
  }

  void _requestNotificationPermission() async {
    final newStatus = await Permission.notification.request();
    if (!mounted) return;
    setState(() {
      _notificationStatus = newStatus;
    });
  }
}
