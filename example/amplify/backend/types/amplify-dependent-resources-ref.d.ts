export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "PinpointDemo": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "analytics": {
        "PinpointDemo": {
            "Region": "string",
            "Id": "string",
            "appName": "string"
        }
    }
}