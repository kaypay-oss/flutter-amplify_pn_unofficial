import 'dart:async';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/services.dart';

class AmplifyPnUnofficial {
  static const MethodChannel _channel = MethodChannel('amplify_pn_unofficial');

  static var _initialized = false;

  static Future<bool> initialize() async {
    assert(
      Amplify.isConfigured,
      'Amplify.configure() must be called beforehand.',
    );
    assert(
      !_initialized,
      '.initialize() can only be called once.',
    );

    final maybeResult = await _channel.invokeMethod<bool?>('initialize');
    return _initialized = maybeResult == true;
  }

  static Future<bool> registerDeviceToken() async {
    assert(
      _initialized,
      '.initialize() must be called beforehand.',
    );

    final result = await _channel.invokeMethod<bool?>('registerDeviceToken');
    return result == true;
  }
}
